# Despliegue de servicios sobre Docker Swarm

## Aprovisionar infraestructura

  Ver instrucciones según proveedor cloud

## Configuracion de servicios

  Editar el fichero docker-compose.yml sustituyendo el servicio _web_ por los servicios reales. Es necesario adaptar las reglas
  del frontend según el caso de uso.

  https://docs.traefik.io/basics/#matchers

## Despliegue de stack (Traefik + Servicios)  

  Conectarse a un nodo del cluster de managers y ejecutar:

  $ sudo docker stack deploy -c docker-compose.yml myswarm

## Despliegue de UI Docker (Portainer)

  $ sudo docker service create \
    --name portainer \
    --publish 9000:9000 \
    --constraint 'node.role == manager' \
     --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    portainer/portainer \
    -H unix:///var/run/docker.sock

  El UI estará accesible en el puerto 9000 a través del LB
