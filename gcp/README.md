# Docker Swarm cluster on Google Cloud Platform

## Instalar y configurar CLI de Google

  https://cloud.google.com/sdk/docs/quickstart-linux

  $ ./google-cloud-sdk/install.sh

  $ gcloud init


## Usuario

Google Cloud Platform -> API Manager -> Credentials -> Create Credentials -> Service account key and chose JSON as key type. Rename the file to account.json and put it in the project root next to main.tf


## Instalar procesador JSON

  $ sudo apt-get install jq
