############################
# Set these in terraform.tfvars
############################

variable "project" {
  description = "ID del proyecto en GCP (no el nombre!)"
}

variable "region" {

}

variable "machine_image" {
  default = "ubuntu-os-cloud/ubuntu-1604-lts"
}
