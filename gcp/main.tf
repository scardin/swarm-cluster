
provider "google" {
  credentials = "${file("account.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}

resource "google_compute_network" "swarm" {
  name                    = "${var.project}-network"
  auto_create_subnetworks = true
  project = "${var.project}"
}

resource "google_compute_firewall" "ssh" {
  name    = "${terraform.workspace}-ssh"
  network = "${google_compute_network.swarm.name}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["swarm"]
}

resource "google_compute_firewall" "http" {
  name    = "${terraform.workspace}-https"
  network = "${google_compute_network.swarm.name}"

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["swarm"]
}
