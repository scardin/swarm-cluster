### security-groups.tf

resource "aws_security_group" "sgswarm" {
  name = "sgswarm"
  description = "Ports required by Docker Swarm nodes"

   vpc_id = "${module.vpc.vpc_id}"

   ingress {
     from_port = 22
     to_port   = 22
     protocol  = "tcp"
     cidr_blocks = ["${var.ssh_access}"]
     description = "Port for SSH traffic"
   }

   ingress {
     from_port = 80
     to_port   = 80
     protocol  = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
     description = "Port for application service routing"
   }

   ingress {
     from_port = 9000
     to_port   = 9000
     protocol  = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
     description = "Port for Portainer UI"
   }

   ingress {
     from_port = 2376
     to_port   = 2376
     protocol  = "tcp"
     cidr_blocks = ["${var.vpc_cidr_block}"]
     description = "Port for the Docker Swarm manager. Used for backwards compatibility"
   }

  ingress {
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
    description = "Port for communication between swarm nodes"
  }

  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
    description = "Port for overlay networking"
  }

  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
    description = "Port for overlay networking"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
    description = "Port for gossip-based clustering"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
    description = "Port for gossip-based clustering"
  }

  egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = [
       "0.0.0.0/0"
     ]
   }
}

resource "aws_security_group" "lb" {
     name = "LB"
     description = "Public ports"

      vpc_id = "${module.vpc.vpc_id}"

      ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      ingress {
        from_port = 9000
        to_port   = 9000
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }

      egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
          "0.0.0.0/0"
        ]
      }

   tags {
     Application = "${var.application}"
   }
 }
