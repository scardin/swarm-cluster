resource "tls_private_key" "example" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "example" {
  key_algorithm   = "${tls_private_key.example.algorithm}"
  private_key_pem = "${tls_private_key.example.private_key_pem}"

  # Certificate expires after 12 hours.
  validity_period_hours = 12

  # Generate a new certificate if Terraform is run within three
  # hours of the certificate's expiration time.
  early_renewal_hours = 3

  # Reasonable set of uses for a server SSL certificate.
  allowed_uses = [
      "key_encipherment",
      "digital_signature",
      "server_auth",
  ]

  dns_names = ["SampleApp-elb-23653230.eu-west-1.elb.amazonaws.com"]

  subject {
      common_name  = "SampleApp-elb-23653230.eu-west-1.elb.amazonaws.com"
      organization = "ACME Examples, Inc"
  }
}

resource "aws_iam_server_certificate" "public_cert" {
  name_prefix      = "example-cert"
  certificate_body = "${tls_self_signed_cert.example.cert_pem}"
  private_key      = "${tls_private_key.example.private_key_pem}"

  lifecycle {
    create_before_destroy = true
  }
}
