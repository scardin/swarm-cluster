# Docker Swarm cluster on AWS

![diagram](architecture.png)

## Instalar y configurar CLI de AWS

  $ pip install awscli

  $ aws configure

  Terraform usa las credenciales aqu� suministradas (se almacenan en ~/.aws/credentials)

## Instalar procesador JSON

  $ sudo apt-get install jq

## Crear AMI Linux con Docker

  Desde directorio _aws/ami_

  $ packer build docker-swarm-ami.json

## Crear par de claves RSA para acceso remoto a EC2

  Desde directorio _aws_

  $ ssh-keygen -b 2048 -t rsa -f keys/ec2 -N ""

## Aprovisionar

  $ terraform init

  $ terraform plan

  $ terraform apply

  $ terraform output
  $ terraform output -module=docker-swarm

## Desaprovisionar

  $ terraform plan -destroy

  $ terraform destroy

## Acceder a EC2

  $ ssh -i keys/ec2 ubuntu@<IP>
