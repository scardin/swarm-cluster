### output.tf

output "ns-servers" {
    value = "${aws_route53_zone.some-zone.name_servers}"
}

output "docker-registry-url" {
    value = "${aws_ecr_repository.swarm.repository_url}"
}

output "elb-dns" {
    value = "${aws_elb.lb.dns_name}"
}

output "rds-endpoint" {
    value = "${aws_db_instance.mydb.endpoint}"
}
