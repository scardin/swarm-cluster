output "manager1_public_ip" {
  value       = "${aws_instance.manager1.public_ip}"
  description = "The manager nodes public ipv4 adresses"
}

output "manager2_public_ip" {
  value       = "${aws_instance.manager2.public_ip}"
  description = "The manager nodes public ipv4 adresses"
}

output "manager3_public_ip" {
  value       = "${aws_instance.manager3.public_ip}"
  description = "The manager nodes public ipv4 adresses"
}

output "manager1_id" {
  value       = "${aws_instance.manager1.id}"
}

output "manager2_id" {
  value       = "${aws_instance.manager2.id}"
}

output "manager3_id" {
  value       = "${aws_instance.manager3.id}"
}

output "manager_token" {
  value       = "${lookup(data.external.swarm_tokens.result, "manager", "")}"
  description = "The Docker Swarm manager join token"
  sensitive = true
}

output "worker_token" {
  value       = "${lookup(data.external.swarm_tokens.result, "worker", "")}"
  description = "The Docker Swarm worker join token"
  sensitive   = true
  sensitive = true
}
