variable "instance_type" {
  description = "EC2 Instance Type"
}

variable "ami" {
  description = "AMI"
}

variable "key_name" {
  description = "File path to SSH private key used to access the provisioned nodes. Ensure this key is listed in the manager and work ssh keys list"
}

variable "vpc_security_groups" {
  description = "Security Groups"
  default = ""
}

variable "subnets" {
  type = "list"
}

variable "ssh_access" {

}

variable "docker_cmd" {
  description = "Docker command"
  default     = "sudo docker"
}

variable "availability" {
  description = "Availability of the node ('active'|'pause'|'drain')"
  default     = "active"
}
