
data "template_file" "provision_first_manager" {
  template = "${file("${path.module}/scripts/provision-first-manager.sh")}"

  vars {
    docker_cmd   = "${var.docker_cmd}"
    availability = "${var.availability}"
  }
}

data "template_file" "provision_manager" {
  template = "${file("${path.module}/scripts/provision-manager.sh")}"

  vars {
    docker_cmd   = "${var.docker_cmd}"
    availability = "${var.availability}"
  }
}

resource "aws_instance" "manager1" {
    ami = "${var.ami}"

    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${var.vpc_security_groups}"]
    subnet_id = "${ var.subnets[0]}"

    connection {
        type          = "ssh"
        user         = "ubuntu"
        private_key   = "${file("keys/ec2")}"
     }

     provisioner "file" {
       content     = "${data.template_file.provision_first_manager.rendered}"
       destination = "/tmp/provision-first-manager.sh"
     }

     provisioner "remote-exec" {
       inline = [
         "chmod +x /tmp/provision-first-manager.sh",
         "/tmp/provision-first-manager.sh ${self.private_ip}",
       ]
     }
}

resource "aws_instance" "manager2" {
    ami = "${var.ami}"

    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${var.vpc_security_groups}"]
    subnet_id = "${ var.subnets[1]}"

    connection {
        type          = "ssh"
        user          = "ubuntu"
        private_key   = "${file("keys/ec2")}"
     }

     provisioner "file" {
       content     = "${data.template_file.provision_manager.rendered}"
       destination = "/tmp/provision-manager.sh"
     }

     provisioner "remote-exec" {
       inline = [
         "chmod +x /tmp/provision-manager.sh",
         "/tmp/provision-manager.sh ${aws_instance.manager1.private_ip} ${lookup(data.external.swarm_tokens.result, "manager")}"
       ]
     }

     depends_on = ["aws_instance.manager1"]
}

resource "aws_instance" "manager3" {
    ami = "${var.ami}"

    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${var.vpc_security_groups}"]
    subnet_id = "${ var.subnets[2]}"

    connection {
        type          = "ssh"
        user          = "ubuntu"
        private_key   = "${file("keys/ec2")}"
     }

     provisioner "file" {
       content     = "${data.template_file.provision_manager.rendered}"
       destination = "/tmp/provision-manager.sh"
     }

     provisioner "remote-exec" {
       inline = [
         "chmod +x /tmp/provision-manager.sh",
         "/tmp/provision-manager.sh ${aws_instance.manager1.private_ip} ${lookup(data.external.swarm_tokens.result, "manager")}"
       ]
     }

     depends_on = ["aws_instance.manager1"]
}

data "external" "swarm_tokens" {
  program    = ["bash", "${path.module}/scripts/get-swarm-join-tokens.sh"]

  query = {
    host        = "${aws_instance.manager1.public_ip}"
    user        = "ubuntu"
    private_key = "./keys/ec2"
  }
}
