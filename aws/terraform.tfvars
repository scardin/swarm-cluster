# This needs to be different for every deployment within a region

application="SampleApp"

domain = "mydomain.com"
static_domain = "s3.mydomain.com"

aws_region="eu-west-1"
aws_azs = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]

# Dowcker Swarm nodes
instance_type="t2.micro"
key_path = "keys/ec2.pub"
ssh_access=["0.0.0.0/0"]

# Database
ddbb_name = "sampleddbb"
ddbb_instance = "db.t2.micro"
ddbb_username = "root"
ddbb_password = "foobarbaz"
