############################
# Set these in terraform.tfvars
############################

variable "application" {
    description = "The name or name prefix of the application and associated resources"
}

variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
}

variable "domain" {
  description = "Public domain"
}

variable "static_domain" {

}

variable "vpc_cidr_block" {
    #https://docs.docker.com/docker-for-aws/faqs/#recommended-vpc-and-subnet-setup
    description = "The VPC CIDR address range"
    default = "172.31.0.0/16"
}

variable "vpc_public_subnets" {
   #https://docs.docker.com/docker-for-aws/faqs/#recommended-vpc-and-subnet-setup
   description = ""
   default = ["172.31.16.0/22","172.31.32.0/22","172.31.48.0/22"]
}

variable "database_subnets" {
   description = ""
   default = ["172.31.20.0/24","172.31.21.0/24"]
}

variable "aws_azs" {
  type="list"
}

variable "ami_name_filter" {
  description = "Search filter for AMI"
  default = "ubuntu-docker*"
}

variable "instance_type" {
  description = "Instance type"
}

variable "key_path" {
  description = "SSH Public Key path"
}

variable "ssh_access" {
    description = "The source IP addresses allowed to SSH onto the cluster instances"
    type="list"
}

variable "ddbb_name" {

}

variable "ddbb_instance" {

}

variable "ddbb_username" {

}

variable "ddbb_password" {

}
