
provider "aws" {
  region = "${ var.aws_region }"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${ var.application}-vpc"
  cidr = "${ var.vpc_cidr_block }"

  azs             = ["${var.aws_azs}"]

  public_subnets  = "${ var.vpc_public_subnets }"
  database_subnets    = "${ var.database_subnets }"

  enable_nat_gateway = false
  enable_vpn_gateway = false
  enable_dns_hostnames = true

  create_database_subnet_group = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

resource "aws_route53_zone" "some-zone" {
  name = "${var.domain}"
}

resource "aws_route53_record" "server1-record" {
  zone_id = "${aws_route53_zone.some-zone.zone_id}"
  name = "${var.domain}"
  type = "A"

  alias {
    name                   = "${aws_elb.lb.dns_name}"
    zone_id                = "${aws_elb.lb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "${var.static_domain}"
  acl    = "public-read"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["https://${var.static_domain}"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}

resource "aws_elb" "lb"{

  name = "${ var.application}-elb"

  subnets         = ["${module.vpc.public_subnets}"]
  security_groups = ["${aws_security_group.lb.id}"]
  internal        = false

  listener {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
  }

  listener {
      instance_port      = 80
      instance_protocol  = "HTTP"
      lb_port            = 443
      lb_protocol        = "HTTPS"
      ssl_certificate_id = "${aws_iam_server_certificate.public_cert.arn}"
  }

  health_check {
      target              = "HTTP:80/"
      interval            = 30
      healthy_threshold   = 2
      unhealthy_threshold = 2
      timeout             = 5
  }

  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}

resource "aws_elb_attachment" "node1" {
  elb      = "${aws_elb.lb.id}"
  instance = "${module.docker-swarm.manager1_id}"
}

resource "aws_elb_attachment" "node2" {
  elb      = "${aws_elb.lb.id}"
  instance = "${module.docker-swarm.manager2_id}"
}

resource "aws_elb_attachment" "node3" {
  elb      = "${aws_elb.lb.id}"
  instance = "${module.docker-swarm.manager3_id}"
}

module "docker-swarm" {
  source = "./modules/docker-swarm"

  instance_type = "${ var.instance_type}"
  ami = "${data.aws_ami.ubuntu-docker.id}"
  key_name = "${aws_key_pair.default.id}"
  vpc_security_groups = "${aws_security_group.sgswarm.id}"
  ssh_access = "${var.ssh_access}"
  subnets = "${ module.vpc.public_subnets }"
}

data "aws_ami" "ubuntu-docker" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${ var.ami_name_filter}"]
  }
}

resource "aws_ecr_repository" "swarm" {
  name = "${lower(var.application)}-ecr"
}

resource "aws_db_instance" "mydb" {
  identifier  = "${lower(var.application)}"
  allocated_storage    = 20
  storage_type         = "gp2"
  multi_az             = false
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "${var.ddbb_instance}"
  name                 = "${var.ddbb_name}"
  username             = "${var.ddbb_username}"
  password             = "${var.ddbb_password}"
  skip_final_snapshot = true
  db_subnet_group_name = "${module.vpc.database_subnet_group}"
}

resource "aws_key_pair" "default"{
  key_name = "${ var.application}-key"
  public_key = "${file("${var.key_path}")}"
}
